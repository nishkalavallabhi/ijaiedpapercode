/**
 * 
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.Tree;
import features.ContentOverlapFeatures;
import features.POSTagBasedFeatures;
import features.ParseTreeBasedFeatures;
import features.PsycholingFeatures;
import features.ReferringExpressionsFeatures;
import features.TraditionalFeatures;
import features.WordBasedFeatures;
import features.WordNetBasedFeatures;
import features.WordlistsBasedFeatures;
import preprocessing.PreprocessText;

/**
 * @author sowmya
 *
 */
public class GetFeaturesForTOEFL11Data {

	/**
	 * Need to get some features for NLI corpus data.
	 * @param args
	 */
	private static TreeMap<String, ArrayList<String>> indexdetails = new TreeMap<String, ArrayList<String>>();
	public static void main(String[] args) throws Exception {
		
		String inputdirpath = "/path/to/corpus/of/txtfiles/";
		String outputcsvfilepath = "features-complexity.csv";
		
		String header = "fileName,prompt,l1,proficiency"; //Header should have file name, all features, prompt, L1, proficiency
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputcsvfilepath));
		File dir = new File(inputdirpath);
		PreprocessText preprocess = new PreprocessText();
		
		loadIndex();
		
		POSTagBasedFeatures pos = new POSTagBasedFeatures();
	    WordBasedFeatures word = new WordBasedFeatures();
     	ParseTreeBasedFeatures parse = new ParseTreeBasedFeatures();
		ContentOverlapFeatures contentfeatures = new ContentOverlapFeatures();
		ReferringExpressionsFeatures refexp = new ReferringExpressionsFeatures();
		WordlistsBasedFeatures lists = new WordlistsBasedFeatures();
		for(File f:dir.listFiles())
		{
			System.out.println("Printing for file: " + f.getName());
			String content = preprocess.getFileContent(f.toString());
			List<?> taggedParsedSentences = preprocess.preProcessFile(content);
			@SuppressWarnings("unchecked") List<List<TaggedWord>> taggedSentences = (List<List<TaggedWord>>) taggedParsedSentences.get(0);
			@SuppressWarnings("unchecked") List<Tree> parsedSentences = (List<Tree>) taggedParsedSentences.get(1);
			@SuppressWarnings("unchecked") ArrayList<String> tokenizedSentences = (ArrayList<String>) taggedParsedSentences.get(2);
			
			TreeMap<String,Double> allFeatures = new TreeMap<String,Double>();
			allFeatures.putAll(pos.getPOSTagBasedFeatures(taggedSentences));
			allFeatures.putAll(word.getWordBasedFeatures(tokenizedSentences));
			allFeatures.putAll(parse.getSyntacticComplexityFeatures(parsedSentences));
			allFeatures.putAll(contentfeatures.getOverlapFeatures(taggedSentences));
			allFeatures.putAll(refexp.getRefExpFeatures(taggedSentences));
			allFeatures.putAll(lists.getWordListsBasedFeatures(tokenizedSentences));
			
			/*String temp = "fileid,prompt,nl,proficiency";
			for(String s:allFeatures.keySet()) //Prints all the features along with names.
			{
				temp += "," + s;	
			}
			System.out.println(temp);
			System.exit(1);
			*/
			String tempname = f.getName();
			String prompt = indexdetails.get(tempname).get(0);
			String l1 = indexdetails.get(tempname).get(1);
			String proficiency = indexdetails.get(tempname).get(2);
			//String temp = f.getAbsolutePath() + "," + prompt + "," + l1 + "," + proficiency;
			String temp = f.getName() + "," + prompt + "," + l1 + "," + proficiency;

			int count  = 0;
			for(String s:allFeatures.keySet()) //Prints all the features along with names.
			{
				temp += "," + allFeatures.get(s);
				
			}
			bw.write(temp);
			bw.newLine();
			count++;
//Note: Header is not printed!
		}
		bw.close();
		System.out.println("Finished processing all files!");
	}

	/*
	 * Specific to TOEFL11 dataset, which comes with a index file describing file name, proficiency, L1 etc.
	 */
	private static void loadIndex() throws Exception
	{
		//Load the index file for training-dev into one Hashmap object.
		String indexPath = "index-all.csv";
		BufferedReader br = new BufferedReader(new FileReader(new File(indexPath)));
		String dummy = "";
		
		while((dummy = br.readLine()) != null)
		{
			String[] tempArray = dummy.split(",");
			
			//File name is the key. Prompt, L1, Proficiency in that order are the list elements.
			indexdetails.put(tempArray[0], new ArrayList<String>(Arrays.asList(new String[]{tempArray[1], tempArray[2], tempArray[3]}))); 
		}
		System.out.println(indexdetails.size());
		br.close();
	}
	
}

