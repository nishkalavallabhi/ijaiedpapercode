This project contains the feature extraction code for:

Automated assessment of non-native learner essays: Investigating the role of linguistic features

Sowmya Vajjala

International Journal of Artificial Intelligence in Education (to appear in  2017)

Code for coreference chain features extraction was from Eyal Schejter. You can contact him (https://bitbucket.org/eschjtr/) for access. 

All other features described in the paper are here. Expected input formats are mentioned in the respective feature code files. 

For questions about the code, contact sowmya -at- iastate dot edu. Read the LICENSE.md file first.  
