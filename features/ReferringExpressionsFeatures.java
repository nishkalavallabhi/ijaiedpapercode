/**
 * 
 */
package features;

import java.util.List;
import java.util.TreeMap;

import edu.stanford.nlp.ling.TaggedWord;
import utils.genutils.NumUtils;

/**
 * Program for computing features based on referential expressions
 * Expects: POS tagged input
 * @author svajjala
 * List of features: 
 *   -Ratio of Pronouns to nouns
 *   -avg proportion of pronouns per sentence and per text
 *   -avg proportion of personal pronouns per sentence and per text
 *   -avg proportion of possessive pronouns per sentence and per text
 *   -avg proportion of definite articles per sentence and per text
 *   -ratio of proper nouns to nouns per text
 */
public class ReferringExpressionsFeatures {

	public ReferringExpressionsFeatures() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public TreeMap<String,Double> getRefExpFeatures(List<List<TaggedWord>> taggedSentences) throws Exception
	{
		TreeMap<String,Double> refExprFeatures = new TreeMap<String,Double>();
		
		//declare all necessary variables:
		double numWords = 0.0; //For getting per text ratios. - numTokens-numNonPunctuations?
		double numSentences = (double)taggedSentences.size(); //For getting per sentence ratios.
		double numPronouns = 0.0;
		double numPersonalPronouns = 0.0;
		double numPossessivePronouns = 0.0;
		double numDefiniteArticles = 0.0; //For now, I am only counting instances of "the" since I don't know how to get it from tagger otherwise.
		double numProperNouns = 0.0;
		double numNouns = 0.0;
		
		for(List<TaggedWord> sentence: taggedSentences)
		{
			for(TaggedWord wordtagpair:sentence)
			{
				String word = wordtagpair.word().toLowerCase();
				String tag = wordtagpair.tag();
				if(Character.isLetter(tag.charAt(0)))
				{
					numWords++;
					if(tag.equals("DT") && word.equals("the"))
					{
						numDefiniteArticles++;
					}
					
					//Pronouns
					else if(tag.equals("PRP"))
					{
						numPersonalPronouns++;
					}
					
					else if(tag.equals("PRP$"))
					{
						numPossessivePronouns++;
					}
					
					//ProperNouns
					else if(tag.startsWith("NN"))
					{
						numNouns++;
						if(tag.startsWith("NNP"))
						{
							numProperNouns++;
						}
					}
				}				
			}
		}
		
		numPronouns = numPersonalPronouns+numPossessivePronouns;
		
		refExprFeatures.put("DISC_RefExprPronounsPerNoun", NumUtils.restrict2TwoDecimals(numPronouns/numNouns));
		refExprFeatures.put("DISC_RefExprPronounsPerSen", NumUtils.restrict2TwoDecimals(numPronouns/numSentences));
		refExprFeatures.put("DISC_RefExprPronounsPerWord", NumUtils.restrict2TwoDecimals(numPronouns/numWords));
		refExprFeatures.put("DISC_RefExprPerPronounsPerSen", NumUtils.restrict2TwoDecimals(numPersonalPronouns/numSentences));
		refExprFeatures.put("DISC_RefExprPerProPerWord", NumUtils.restrict2TwoDecimals(numPersonalPronouns/numWords));
		refExprFeatures.put("DISC_RefExprPossProPerSen", NumUtils.restrict2TwoDecimals(numPossessivePronouns/numSentences));
		refExprFeatures.put("DISC_RefExprPossProPerWord", NumUtils.restrict2TwoDecimals(numPossessivePronouns/numWords));
		refExprFeatures.put("DISC_RefExprDefArtPerSen", NumUtils.restrict2TwoDecimals(numDefiniteArticles/numSentences));
		refExprFeatures.put("DISC_RefExprDefArtPerWord", NumUtils.restrict2TwoDecimals(numDefiniteArticles/numWords));
		refExprFeatures.put("DISC_RefExprProperNounsPerNoun", NumUtils.restrict2TwoDecimals(numProperNouns/numNouns));
		
		return refExprFeatures;
	}

}
