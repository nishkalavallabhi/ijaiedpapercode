/**
 * 
 */
package features;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.languagetool.JLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.RuleMatch;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import preprocessing.PreprocessText;
import utils.genutils.NumUtils;

/**
 * @author sowmya
 *
 */
public class SpellingGrammarCheck {

	/**
	 * uses LanguageTool to do a spelling and grammar check 
	 * Features to get:
	 * 1. Avg Num of spelling mistakes per sentence
	 * 2. Avg Num of all (including spelling) mistakes per sentence
	 * 3. Percentage of words that are spelling errors.
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		String inputDir = "/dir/containing/txt/files";
		//String inputDir = "/Users/sowmya/dummyFolder";
		String outputFile = "/feature/file/with/error/features.csv";
			
	    String header = "fileid,ERR_00_SpellingErrors,ERR_01_OtherErrors,ERR_02_AllErrors";	 	
	    
	    BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		bw.write(header);
		bw.newLine();
		
		SpellingGrammarCheck err = new SpellingGrammarCheck();
		PreprocessText pp = new PreprocessText();
		
		File[] files = new File(inputDir).listFiles();
		for(File f: files)
		{
			String name = f.getName().replace("disc","txt");
			String content = pp.getFileContent(f.toString());
			ArrayList<String> sentences = tokenizedSentences(content);
			TreeMap<String,Double> featuresForThisFile = err.getSpellingGrammarFeatures(sentences);
			String toWrite = name ;
			for(String s:featuresForThisFile.keySet()) //Prints all the features along with names.
			{
				toWrite += "," + featuresForThisFile.get(s);
			}   
			bw.write(toWrite);
			bw.newLine();
			System.out.println("Finished writing for: " + f.getName());
		}
		bw.close();
	}

	
	public TreeMap<String,Double> getSpellingGrammarFeatures(ArrayList<String> sentences) throws Exception
	{
		TreeMap<String,Double> checkFeatures = new TreeMap<String,Double>();
		
		int numSentences = sentences.size();
		int numSpellingMistakes = 0;
		int numOtherMistakes = 0;
		int numAllMistakes = 0;
		
	 	JLanguageTool jlt = new JLanguageTool(new AmericanEnglish());

		
		for(String sentence:sentences)
		{
			List<RuleMatch> matches = jlt.check(sentence);
			for (RuleMatch match : matches)
			{
				if(match.getMessage().contains("spelling mistake"))
				{
					numSpellingMistakes++;
				}
				else
				{
					numOtherMistakes++;
				}
			}
			
		}
		numAllMistakes = numSpellingMistakes + numOtherMistakes;
		
		checkFeatures.put("ERR_00_SpellingErrorsPerSen", NumUtils.restrict2TwoDecimals((double)numSpellingMistakes/numSentences));
		checkFeatures.put("ERR_01_OtherErrors", NumUtils.restrict2TwoDecimals((double)numOtherMistakes/numSentences));
		checkFeatures.put("ERR_02_AllErrors", NumUtils.restrict2TwoDecimals((double)numAllMistakes/numSentences));
		checkFeatures.put("ERR_02_PercentSpellingErrorsWrtAllErrors", NumUtils.restrict2TwoDecimals((double)numSpellingMistakes/numAllMistakes));

		
		return checkFeatures;
	}
	
	private static ArrayList<String> tokenizedSentences(String content) throws Exception
	{
		DocumentPreprocessor tokenizer = new DocumentPreprocessor(new StringReader(content));
		ArrayList<String> sentences = new ArrayList<>();
		for (List<HasWord> sentence : tokenizer) 
        {
			sentences.add(PreprocessText.listToString(sentence));
        }
		return sentences;
	}
}
