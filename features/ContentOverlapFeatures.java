/**
 * 
 */
package features;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import utils.genutils.NumUtils;

/**
 * Content Overlap features as described in Coh-Metrix: Noun-overlap, Argument overlap, Stem overlap, Content word overlap.
 * Local and Global versions for each - as described in Coh-Metrix.
 * Noun-overlap: Sentences that have atleast one overlapping noun => Binary feature.
 * Argument overlap: BINARY
 * Stem overlap: BINARY
 * Content word overlap: proportion feature.
 * @author svajjala
 * TODO: I am not sure which is the best ratio to pick yet. For now, I am dividing everything by num. Sentences.
 */
public class ContentOverlapFeatures {

	public ContentOverlapFeatures() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		//Example usage:		
		String text = "Rama gave me an apple. Along with it, he gave me a muffin. Rama then gave me something else. But I did not give him anything.";
		MaxentTagger tagger = new MaxentTagger("models/english-left3words-distsim.tagger");
		DocumentPreprocessor tokenizer = new DocumentPreprocessor(new StringReader(text));
		List<List<TaggedWord>> taggedSentences = new ArrayList<List<TaggedWord>>();
		for (List<HasWord> sentence : tokenizer) {
		        taggedSentences.add(tagger.tagSentence(sentence));
		 }
	     System.out.println(taggedSentences.size());
		 ContentOverlapFeatures content = new ContentOverlapFeatures();
		 TreeMap<String,Double> eg = content.getOverlapFeatures(taggedSentences);
		 for(String s:eg.keySet()) //Prints all the features along with names.
		 {
			System.out.println(s + "  " + eg.get(s)); 
		  }    

	}
	
	public TreeMap<String,Double> getOverlapFeatures(List<List<TaggedWord>> taggedSentences) throws Exception
	{
		TreeMap<String, Double> overlapFeatures = new TreeMap<String,Double>();
		List<ArrayList<ArrayList<String>>> sentences = returnListOfFormattedSentences(taggedSentences);
		
		double localNounOverlapCount = 0; 
		double localArgumentOverlapCount = 0;
		double localStemOverlapCount = 0;
		double localContentWordOverlap = 0;
		
		double globalNounOverlapCount = 0; 
		double globalArgumentOverlapCount = 0;
		double globalStemOverlapCount = 0;
		double globalContentWordOverlap = 0;
		
		int totalSentencesSize = taggedSentences.size(); //denominator for local overlap features.
	//	int totalPairsCount = 0; //Used as the denominator for global overlap features.
		
		//For Local Overlap
		for(int i=0;i<totalSentencesSize;i++)
		{
			for(int j=i+1;j<totalSentencesSize;j++)
			{
				ArrayList<ArrayList<String>> sentence1 = sentences.get(i);
				ArrayList<ArrayList<String>> sentence2 = sentences.get(j);
				if(isThereNounOverlap(sentence1, sentence2))
				{
					if(j-i ==1)
					{
						localNounOverlapCount++;
						localArgumentOverlapCount++;
						localStemOverlapCount++;
					}
					globalNounOverlapCount++;
					globalArgumentOverlapCount++;
					globalStemOverlapCount++;
				}
				else if(isThereArgumentOverlap(sentence1, sentence2))
				{
					if(j-i ==1)
					{
						localArgumentOverlapCount++;
						localStemOverlapCount++;
					}
					globalArgumentOverlapCount++;
					globalStemOverlapCount++;
				}
				else if(isThereStemOverlap(sentence1, sentence2))
				{
					if(j-i ==1)
					{
						localStemOverlapCount++;
					}
					globalStemOverlapCount++;
				}
				
				double tempContentOverlap = contentWordOverlap(sentence1, sentence2);
				globalContentWordOverlap += tempContentOverlap;
				if(j-i ==1)
				{
					localContentWordOverlap += tempContentOverlap;
				}
				//totalPairsCount++;
			}//End per sentence
		}//End of all sentences
		overlapFeatures.put("DISC_localNounOverlapCount", NumUtils.restrict2TwoDecimals(localNounOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_localArgumentOverlapCount", NumUtils.restrict2TwoDecimals(localArgumentOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_localStemOverlapCount", NumUtils.restrict2TwoDecimals(localStemOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_localContentWordOverlapCount", NumUtils.restrict2TwoDecimals(localContentWordOverlap/totalSentencesSize));
		overlapFeatures.put("DISC_globalNounOverlapCount", NumUtils.restrict2TwoDecimals(globalNounOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_globalArgumentOverlapCount", NumUtils.restrict2TwoDecimals(globalArgumentOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_globalStemOverlapCount", NumUtils.restrict2TwoDecimals(globalStemOverlapCount/totalSentencesSize));
		overlapFeatures.put("DISC_globalContentWordOverlapCount", NumUtils.restrict2TwoDecimals(globalContentWordOverlap/totalSentencesSize));
		return overlapFeatures;
	}
	
	/**
	 * Checks for Noun Overlap, as per Coh-Metrix definition.
	 * @param sentence1
	 * @param sentence2
	 * @return
	 * @throws Exception
	 */
	private static boolean isThereNounOverlap(ArrayList<ArrayList<String>> sentence1, ArrayList<ArrayList<String>> sentence2) throws Exception
	{
		for(ArrayList<String> word:sentence1)
		{
			if(word.get(3).equals("NOUN") && sentence2.contains(word))
				{
					return Boolean.TRUE;
				}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Checks for stem Overlap, if there are no noun or argument overlaps, as per Coh-Metrix definition.
	 * @param sentence1
	 * @param sentence2
	 * @return
	 * @throws Exception
	 */
	private boolean isThereStemOverlap(ArrayList<ArrayList<String>> sentence1, ArrayList<ArrayList<String>> sentence2) throws Exception
	{
		if(isThereNounOverlap(sentence1, sentence2) || isThereArgumentOverlap(sentence1, sentence2))
		{
			return Boolean.TRUE; //If there is a noun or argument overlap, there is obviously an argument overlap right? No need to test further.
		}
		else
		{
			for(ArrayList<String> word:sentence1)
			{
				if(!word.get(3).equals("NOTAG"))
				{
					String lemmaTemp = word.get(1);
					String posTemp = word.get(3);
					for(ArrayList<String> wordTemp:sentence2)
					{
						String lemmaTemp2 = wordTemp.get(1);
						String posTemp2 = wordTemp.get(3);
						if(lemmaTemp.equals(lemmaTemp2) && (posTemp2.equals("NOUN") || (posTemp.equals("NOUN")) || posTemp.equals("PRONOUN")))
						{
							return Boolean.TRUE;
						}
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Checks for argument Overlap if there is no noun overlap, as per Coh-Metrix definition.
	 * @param sentence1
	 * @param sentence2
	 * @return
	 * @throws Exception
	 */
	private boolean isThereArgumentOverlap(ArrayList<ArrayList<String>> sentence1, ArrayList<ArrayList<String>> sentence2) throws Exception
	{
		if(isThereNounOverlap(sentence1, sentence2))
		{
			return Boolean.TRUE; //If there is a noun overlap, there is obviously an argument overlap right? No need to test further.
		}
		//Check for pronoun overlap?
		else
		{
			for(ArrayList<String> word:sentence1)
			{
				if(word.get(3).equals("PRONOUN") && sentence2.contains(word))
				{
						return Boolean.TRUE;
				}
				else 
				{
				  if(word.get(3).equals("NOUN")||word.get(3).equals("PRONOUN"))
				  {
					String lemmaTemp = word.get(1);
					String posTemp = word.get(3);
					for(ArrayList<String> wordTemp:sentence2)
					{
						if(lemmaTemp.equals(wordTemp.get(1)) && posTemp.equals(wordTemp.get(3)) && !posTemp.equals("NOTAG"))
						{
							return Boolean.TRUE;
						}
					}
				  }
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Calculates content word overlap between two sentences.
	 * @param sentence1
	 * @param sentence2
	 * @return overlap/sen.len.
	 * @throws Exception
	 */
	public double contentWordOverlap(ArrayList<ArrayList<String>> sentence1, ArrayList<ArrayList<String>> sentence2) throws Exception
	{
		int overlapsCount = 0;
		//Isn't this similar to cosine similarity between content-words?
		for(ArrayList<String> word:sentence1)
		{
			String poscatTemp = word.get(3);
			String lemmaTemp = word.get(1);
			if(!poscatTemp.equals("NOTAG") && !poscatTemp.equals("PRONOUN"))
			{
				for(ArrayList<String> word2:sentence2)
				{
					if(lemmaTemp.equals(word2.get(1)))
					{
						overlapsCount++;
					}
				}
			}			
		}
		return (double)overlapsCount;
	}
	
	/**
	 * Take fulltext, splits it to sentences, then returns each sentence in the datastructure format needed for further processing.
	 * @param fulltext
	 * @return
	 * @throws Exception
	 */
	private static List<ArrayList<ArrayList<String>>> returnListOfFormattedSentences(List<List<TaggedWord>> taggedSentences) throws Exception
	{
	    List<ArrayList<ArrayList<String>>> result = new ArrayList<ArrayList<ArrayList<String>>>();
	    for(List<TaggedWord> sentence:taggedSentences)
	    {
	    	result.add(returnFormattedSentence(sentence));
	    }
		return result;
	}
	
	/**
	 * Takes a sentence, tags it, and returns a formatted version suitable for the ContentOverlapFeaturesExtractor method
	 * @param taggedSentence 
	 * @param result: ArrayList<ArrayList<String>> - Each item in this list is a list consisting of <word, lemma, POS, general tag>
	 * @throws Exception
	 */
	private static ArrayList<ArrayList<String>> returnFormattedSentence(List<TaggedWord> taggedSentence) throws Exception
	{
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		Morphology morph = new Morphology();
		for(TaggedWord wordTagPair:taggedSentence)
		{
			ArrayList<String> temp = new ArrayList<String>();
			temp.add(wordTagPair.word().toLowerCase());
			temp.add(morph.lemma(wordTagPair.word(), wordTagPair.tag()));
			temp.add(wordTagPair.tag());
			temp.add(getGeneralTag(wordTagPair.tag()));
			result.add(temp);
		}
		return result;
	}
	
	
	/**
	 * 
	 * @param specificTag
	 * @return
	 * @throws Exception
	 */
	private static String getGeneralTag(String specificTag) throws Exception
	{
		String generaltag = "NOTAG";
		if(specificTag.startsWith("VB"))
		{
			generaltag = "VERB";
		}
	
		else if(specificTag.startsWith("JJ"))
		{
			generaltag = "ADJECTIVE";
		}
		else if(specificTag.startsWith("RB") || specificTag.equals("WRB"))
		{
			generaltag = "ADVERB";
		}
		else if(specificTag.startsWith("PRP") || specificTag.startsWith("WP"))
		{
		generaltag = "PRONOUN";
		}
		else if(specificTag.startsWith("NN"))
		{
			generaltag = "NOUN";
		}
		return generaltag;
	}
	
	
	
}
