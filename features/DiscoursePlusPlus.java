/**
 * 
 */
package features;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.ParseException;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.trees.tregex.TregexPatternCompiler;
import preprocessing.PreprocessText;
import utils.genutils.NumUtils;

/**
 * @author sowmya
 * Purpose: To extract features based on discourse connectives usage, using the discourse connectives tagger from Upenn
 * http://www.cis.upenn.edu/~nlp/software/discourse.html
 * I am just using the output of their program to extract some of the features. 
 */
public class DiscoursePlusPlus {

	/**
	 * Takes as input the output of addDiscourse.pl program and outputs 6 features per text: 
	 * 1. num. discourse connectives (annotated with one of the four categories)
	 * 2. num. non-discourse connectives (annotated as 0)
	 * 3. num. comparision connectives
	 * 4. num. expansion connectives
	 * 5. num. contingency connectives
	 * 6. num. temporal connectives.
	 * 7. num. connectives per text
	 * how to calculate the features: as denominator- number of all connectives (7 which is 1+2) for 1-6 and num. word tokens for 7. 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
	
		String inputDir = "/path/to/your/outputfiles/from/discconnectivestagger/"; //Each file in this directory is the output file from discourse connectives tagger.
		String outputFile = "/path/to/your/outputfiles/from/discconnectivestagger/discourse-connectives.csv";
		
		String header = "fileid,numConnectives,numDiscConnectives,numNonDiscConnectives,numCompConnectives,numExpConnectives,numContConnectives,numTempConnectives";
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		bw.write(header);
		bw.newLine();

		DiscoursePlusPlus dpp = new DiscoursePlusPlus();
		
		File[] files = new File(inputDir).listFiles();
		for(File f: files)
		{
			ArrayList<String> content = dpp.getFileContent(f.toString());
			String name = f.getName().replace("disc","txt");
			TreeMap<String,Double> featuresForThisFile = dpp.getDiscoursePlusFeatures(content);
			String toWrite = name ;
			for(String s:featuresForThisFile.keySet()) //Prints all the features along with names.
			{
				toWrite += "," + featuresForThisFile.get(s);
			}   
			bw.write(toWrite);
			bw.newLine();
		}
		bw.close();
	}
	
	public TreeMap<String,Double> getDiscoursePlusFeatures(ArrayList<String> content) throws Exception
	{
		TreeMap<String,Double> discPlusFeatures = new TreeMap<String,Double>();
		int numSentences = content.size();
		
		int numConnectives = 0;
		int numDiscConnectives = 0;
		int numNonDiscConnectives = 0;
		int numCompConnectives = 0;
		int numExpConnectives = 0;
		int numContConnectives = 0;
		int numTempConnectives = 0;
		
		for(String parsed:content)
		{
			numNonDiscConnectives += countOccurences(parsed, "#0\\)");
			numCompConnectives += countOccurences(parsed, "#Comparison\\)");
			numExpConnectives += countOccurences(parsed, "#Expansion\\)");
			numContConnectives += countOccurences(parsed, "#Contingency\\)");
			numTempConnectives += countOccurences(parsed, "#Temporal\\)");
		}
		
		numDiscConnectives = numCompConnectives + numExpConnectives + numContConnectives + numTempConnectives;
		numConnectives = numDiscConnectives + numNonDiscConnectives;
		
		discPlusFeatures.put("DISCPlus_00_numConnectivesPerSen", NumUtils.restrict2TwoDecimals((double)numConnectives/numSentences));
		discPlusFeatures.put("DISCPlus_01_numDiscConnectivesPerSen", NumUtils.restrict2TwoDecimals((double)numDiscConnectives/numSentences));
		discPlusFeatures.put("DISCPlus_02_numNonDiscConnectivesPerSen", NumUtils.restrict2TwoDecimals((double)numNonDiscConnectives/numSentences));
		discPlusFeatures.put("DISCPlus_03_numCompConnectivesPerSen", NumUtils.restrict2TwoDecimals((double)numCompConnectives/numSentences));
		discPlusFeatures.put("DISCPlus_04_numExpConnectivesPerSen", NumUtils.restrict2TwoDecimals((double)numExpConnectives/numSentences));
		discPlusFeatures.put("DISCPlus_05_numContConnectives", NumUtils.restrict2TwoDecimals((double)numContConnectives/numSentences));
		return discPlusFeatures;
	}
	
	
	
	/*
	 * Returns the file content as an ArrayList of discourse parsed trees per sentence.
	 */
	public ArrayList<String> getFileContent(String filePath) throws Exception
	{
		ArrayList<String> finalList = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String dummy = "";
		while((dummy = br.readLine()) != null)
		{
			finalList.add(dummy);
		}
		br.close();
		return finalList;
	}
	
	  private static int countOccurences(String parsed, String pattern) throws ParseException
	    {
	    	Pattern p0 = Pattern.compile(pattern);
	    	Matcher m0 = p0.matcher(parsed);
	    	int count = 0;
	    	while(m0.find())
			{
			     count++;
			}
	    	return count;
	    }

}
